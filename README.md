# Wordpress to Graphene Blockchain
---

The `wordpress-graphene` library offers a powerful tool for bloggers and content creators using WordPress. 

It simplifies the process of extending the reach of their content beyond traditional web platforms. Specifically, it facilitates the automatic publishing of new blog posts onto the Blurt blockchain. 

This can be achieved in two ways: first, by retrieving the posts through RSS feeds, which are commonly used to distribute content updates; and second, by directly calling the WordPress API to fetch new posts. Once these posts are published on the Blurt blockchain, they become part of a unique ecosystem where content is valued and rewarded. 

Authors can earn BLURT tokens as a form of reward, which are distributed based on the community's engagement with the content, primarily through upvotes. 

This not only provides a new avenue for monetizing content but also introduces WordPress bloggers to the burgeoning world of blockchain and decentralized finance (DeFi), offering them a way to participate in and benefit from the digital economy.

The posts are retrieved using the WordPress API of the blog (to obtain the post cover image). If the API is unavailable or disabled, a fallback to the RSS feed is used. The posts are then converted to Markdown before being published.

The application automatically restarts in case of an error.

A copy of the operations in .json format is saved locally in the data/posts folder.

---
## Getting started

### Install

clone the repository

```bash
$  git clone https://gitlab.com/beblurt/wordpress-graphene.git
```

Install the dependencies

```bash
$  npm install
```

Build for NodeJS ESM Typescript... in `"./dist"` directory (requires Typescript to be installed 👉 `npm install -g typescript`)

```bash
$  npm run build
```

or

```bash
$ tsc
```

### Configure

Run the command

```bash
$ npm run configure
```

or manually

```bash
$ node ./dist/configure
```
Answer the questions (validate for default value)

#### BLURT TESTNET

```bash
? There is a config for the BLURT TESTNET? (Y/n)
```

Answer `Y` (default: Yes) if you want to configure the settings of the TESTNET

```bash
? BLURT TESTNET ADDRESS PREFIX (TST)
```

Enter the BLURT TESTNET address prefix (default: "TST")

```bash
? BLURT TESTNET CHAIN ID (1df54a5cc86f7c7efee2402e1304df6eae24eb8766a63c0546c1b2511cf5eba6)
```

Enter the BLURT TESTNET chain ID (default: "1df54a5cc86f7c7efee2402e1304df6eae24eb8766a63c0546c1b2511cf5eba6")

```bash
? BLURT TESTNET NODES RPC (separated by comma if many) (https://testnet-rpc.beblurt.com) 
```

Enter a list of RPC nodes separated by comma for the BLURT TESTNET (default: "https://testnet-rpc.beblurt.com")

```bash
? BLURT TESTNET PUBLISHER ACCOUNT 
```

Enter the name of the BLURT TESTNET account that will publish the posts 

*note: a check is performed on the blockchain to verify the validity of the information.*

```bash
? BLURT TESTNET PUBLISHER ACCOUNT 
```

Enter the name of the BLURT TESTNET account that will publish the posts 

*note: a check is performed on the blockchain to verify the validity of the information.*

```bash
? BLURT TESTNET PUBLISHER ACCOUNT POSTING KEY [input is hidden]
```

Enter the posting key of the BLURT TESTNET account that will publish the posts (value not displayed)

*note: a check is performed on the blockchain to verify the validity of the information.*

#### BLURT MAIN CHAIN

```bash
? There is a config for the BLURT MAIN CHAIN? (Y/n)
```

Answer `Y` (default: Yes) if you want to configure the settings of the BLURT MAIN CHAIN

```bash
? BLURT MAIN CHAIN PREFIX (BLT)
```

Enter the BLURT MAIN CHAIN address prefix (default: "BLT")

```bash
? BLURT MAIN CHAIN CHAIN ID (cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f)
```

Enter the BLURT MAIN CHAIN chain ID (default: "cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f")

```bash
? BLURT MAIN CHAIN NODES RPC (separated by comma if many) (https://rpc.beblurt.com,https://rpc.blurt.world) 
```

Enter a list of RPC nodes separated by comma for the BLURT MAIN CHAIN (default: "https://rpc.beblurt.com", "https://rpc.blurt.world")

```bash
? BLURT MAIN CHAIN PUBLISHER ACCOUNT 
```

Enter the name of the BLURT MAIN CHAIN account that will publish the posts 

*note: a check is performed on the blockchain to verify the validity of the information.*

```bash
? BLURT MAIN CHAIN PUBLISHER ACCOUNT 
```

Enter the name of the BLURT MAIN CHAIN account that will publish the posts 

*note: a check is performed on the blockchain to verify the validity of the information.*

```bash
? BLURT MAIN CHAIN PUBLISHER ACCOUNT POSTING KEY [input is hidden]
```

Enter the posting key of the BLURT MAIN CHAIN account that will publish the posts (value not displayed)

*note: a check is performed on the blockchain to verify the validity of the information.*

#### HTTP (backend)

```bash
? What PORT to use for HTTP? (3000)
```

Enter the TCP port to use for HTTP on the backend (default: 3000)

```bash
? Generate an Authentication Token for HTTP Header Request? (Y/n)
```

Generate an Authentication Token for communication with the backend (default: Yes)

```bash
? Authentication Token for HTTP Header Request 
```

Only if you answered no to the previous question! Enter an Authentication Token for communication on with backend

### Post footer

You can edit the `POST_FOOTER.md` file in the `data` folder to change the footer of the posts

## Start the backend

### With npm

For the MAIN CHAIN, run the command

```bash
$  npm start
```

![MAIN CHAIN](https://img.blurt.world/blurtimage/nalexadre/63d3327a89bacc155a2ecc8566916ac84d0e0ae2.png)

For the TESTNET, run the command

```bash
$  npm run testnet
```

![TESTNET](https://img.blurt.world/blurtimage/nalexadre/70095e49b28874a4f24fc03c1357b98a01de6548.png)

### Manually

run the command

```bash
$  node ./dist [options]
```

To see the option run the command

```bash
$  node ./dist -h

Usage: dist [options]

Options:
  -V,      --version                    output the version number
  -env,    --environment <ENVIRONMENT>  Environment: TESTNET | MAIN_CHAIN (default: "MAIN_CHAIN")
  -d,      --debug                      output extra debugging
  -h,      --help                       display help for command
```

### PM2

if you want to use PM2 (https://pm2.keymetrics.io/)

### MAIN CHAIN

For the MAIN CHAIN run the command

```bash
$  pm2 start pm2-main-chain.cjs
```

### TESTNET

For the TESTNET run the command

```bash
$  pm2 start pm2-testnet.cjs
```

## Feeds

### Add a feed

To add a feed run the command

```bash
$  npm run add-feed #for the main chain
$  npm run add-feed-testnet #for the testnet
```

or manually

```bash
$  node ./dist/add [options]
```

To see the option run the command

```bash
$  ./dist/add -h

Usage: dist [options]

Options:
  -V,      --version                    output the version number
  -env,    --environment <ENVIRONMENT>  Environment: TESTNET | MAIN_CHAIN (default: "MAIN_CHAIN")
  -d,      --debug                      output extra debugging
  -h,      --help                       display help for command
```

Answer the questions (validate for default value)

```bash
? Blog URL? (e.g. https://my.blog.com)
```

Enter the root url of the blog (e.g. https://my.blog.com)

```bash
? Main tag? (e.g. blurt)
```

Enter a blockchain tag (e.g. blurt)

```bash
? verification URL?
```

Enter the verification URL

```bash
? Beneficiary account? (without @)
```

Enter the main beneficiary account

*note: a check is performed on the blockchain to verify the validity of the information.*

```bash
? Percentage? (e.g. 3)
```

Enter a % for the beneficiary (e.g. 3)

```bash
? Add more beneficiaries? (Y/n)
```

If you want to add more beneficiaries (default: Yes)

```bash
########################################
## FEED TO ADD
########################################

{
  _id: '9999999999999999999999999999999999999999999999999999999999999999',
  feedType: 'API',
  blogURL: 'https://my.blog.com',
  mainTag: 'blurt',
  verificationURL: 'https://my.blog.com/verif',
  beneficiaries: [
    { account: 'my-account', value: 3 },
    { account: 'another-account', value: 3 }
  ],
  last_batch: '1970-01-01T00:00:00.000Z'
}
? All the information are correct? (Y/n) 
```

Validate the information (default: Yes)

```bash
Feed: 10 posts for https://web2.support/feed => API: OK / RSS: OK
Feed https://web2.support/feed added!
```

Response from the backend. Congrats you added your first Wordpress Blog

---

## Support

You can contact me in the "dev" channel of the Official Blurt Discord server: https://discord.blurt.world/

## Contributing

Pull requests for new features, bug fixes, and suggestions are welcome!

## Author

@nalexadre (https://beblurt.com/@nalexadre)

## License

Copyright (C) 2024  IMT ASE Co., LTD

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.