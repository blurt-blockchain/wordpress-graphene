/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'
import * as readline from 'readline'

/** Version */
import { createRequire } from "module"
const require            = createRequire(import.meta.url)
const packageJson        = require('../package.json')
process.env['VERSION']   = packageJson.version

/** Require */
import crypto from 'crypto'
import inquirer, { Answers } from 'inquirer'
import axios, { AxiosRequestConfig } from 'axios'

/** dblurt */
import { cryptoUtils } from '@beblurt/dblurt'

/** libs */
import { configGet, checkBlockchainAccount } from "./libs/utils.js"

/** Helpers */
import { RegExpUrl, BENEFICIARY, FEED } from './modules/feed/helpers/feed.js'
import { ENV, RegExpEnv } from './helpers/config.js'

/** Add beneficiary function */
const addBeneficiary = async (rpcs: string[]): Promise<BENEFICIARY> => {
  const beneficiary: Answers = await inquirer.prompt([
    {
      type: 'input',
      name: 'account',
      message: 'Beneficiary account? (without @)',
      validate: async (value) => {
        const r = new RegExp(cryptoUtils.regExpAccount)
        if (r.test(value)) {
          return await checkBlockchainAccount(value, rpcs) ? true : 'Not exist on Blockchain!'
        } else {
          return 'Invalid account!'
        }
      },
    },
    {
      type: 'input',
      name: 'value',
      message: 'Percentage? (e.g. 3)',
      validate(value) {
        const pass = value.match(/^([1-9]?\d|100)$/)
        if (pass) {
          return true;
        }
        return 'Invalid Percentage';
      },
    },
  ])
  return { account: beneficiary['account'], weight: Number(beneficiary['value']) * 100 }
}

const removeTrailingSlash = (url: string): string => {
  return url.replace(/\/$/, "")
}

/** Commander */
import { Command } from 'commander'
const program = new Command()
program
  .version(packageJson.version)
  .option('-env,    --environment <ENVIRONMENT>',   'Environment: TESTNET | MAIN_CHAIN', 'MAIN_CHAIN')
  .option('-d,      --debug',                       'output extra debugging');

/** Add a feed to the list */
(async () => {
  try {
    console.log(`\x1b[0;32m${ 'Wordpress Graphene' }\x1b[0m version \x1b[0;33m${ packageJson.version }\x1b[0m`)
    console.log()

    program.parse(process.argv)
    const options = program.opts()

    /** Environment */
    assert(options["environment"].match(RegExpEnv), `Environment not exist! Accept only => TESTNET | MAIN_CHAIN`)
    const environment:ENV = options["environment"]
    console.log(`Environment: ${ options["environment"] === 'TESTNET' ? '\x1b[0;33mTESTNET\x1b[0m' : '\x1b[0;35mMAIN CHAIN\x1b[0m' }`)
    console.log()

    /** Config */
    const config = await configGet()
    assert(environment === 'TESTNET' ? config?.BLURT.TESTNET : config?.BLURT.MAIN_CHAIN, `Empty ${environment === 'TESTNET' ? 'TESTNET' : 'MAIN_CHAIN'} config!`)
    const rpcs: string[] = environment === 'TESTNET' ? config!.BLURT.TESTNET!.NODES_RPC : config!.BLURT.MAIN_CHAIN!.NODES_RPC

    /** Main questions */
    console.log(`\x1b[0;35m${ 'Add a Worpress Blog' }\x1b[0m`)
    const addFeed: Answers = await inquirer.prompt([
      {
        type:    'input',
        name:    'blogURL',
        message: 'Blog URL? (e.g. https://my.blog.com)',
        validate(value) {
          const pass = value.match(RegExpUrl)
          if (pass) {
            return true
          }
          return 'Invalid URL';
        },
      },
      {
        type: 'input',
        name: 'tag',
        message: 'Main tag? (e.g. blurt)',
        validate(value) {
          const pass = value.match(/^\w+$/)
          if (pass) {
            return true;
          }
          return 'Invalid tag';
        },
      },
      {
        type: 'input',
        name: 'verificationURL',
        message: 'verification URL?',
        validate(value) {
          const pass = value.match(RegExpUrl)
          if (pass) {
            return true
          }
          return 'Invalid URL';
        },
      },
    ])

    /** Add beneficiary loop */
    let lastBeneficiary = false
    let beneficiaries: BENEFICIARY[] = []
    while(!lastBeneficiary) {
      beneficiaries.push(await addBeneficiary(rpcs))
      const question: Answers = await inquirer.prompt([
        {
          type: 'confirm',
          name: 'add',
          message: 'Add more beneficiaries?',
          default: true,
        },
      ])
      if(!question['add']) {
        lastBeneficiary = true
      }
    }

    /** Prepare the feed to add */
    const feed: FEED = {
      _id :            crypto.createHmac('sha256', environment).update(addFeed['blogURL']).digest('hex'),
      feedType:        'API',
      blogURL:         removeTrailingSlash(addFeed['blogURL']),
      mainTag:         addFeed['tag'],
      verificationURL: addFeed['verificationURL'],
      beneficiaries,
      last_batch: (new Date('1970-01-01')).toISOString() 
    }

    /** Show the feed to add */
    console.log()
    console.log('########################################')
    console.log('## FEED TO ADD')
    console.log('########################################')
    console.log()
    console.log(feed)

    /** Ask to validate */
    const validate: Answers = await inquirer.prompt([
      {
        type: 'confirm',
        name: 'validate',
        message: 'All the information are correct?',
        default: true,
      }
    ])

    /** Send to the backend */
    if(validate['validate']) {
      const options: AxiosRequestConfig = {
        method: 'POST',
        url:    `http://127.0.0.1:${config!.HTTP.PORT}/rpc/`,
        headers: {
          accept:            "application/json",
          "Accept-Encoding": "gzip,deflate,compress",
          authorization:     `Bearer ${config!.HTTP.TOKEN}`,
        },
        data: {
          jsonrpc: '2.0',
          method:  'feed.add',
          params:  { 
            feed,
            signature: [] 
          },
          id: Date.now()
        }
      }

      /** Spinner */
      const P = ['\\', '|', '/', '-'];
      let x = 0;
      const loader = setInterval(() => {
        readline.moveCursor(process.stdout, -1, 0)
        process.stdout.write(`${P[x++]}`);
        x %= P.length;
      }, 250)

      const response = await axios(options)

      /** Spinner */
      clearInterval(loader)
      
      process.stdout.clearLine(0)
      process.stdout.write('\n')

      if(response.data?.error) {
        process.stdout.write(`\x1b[0;31m${ response.data?.error?.message }\x1b[0m`)
        process.stdout.write('\n')
      } else {
        process.stdout.write(`${response.data?.result[0]}`)
        process.stdout.write('\n')
        process.stdout.write(`Feed \x1b[0;35m${ feed.blogURL }\x1b[0m added!`)
        process.stdout.write('\n')
      }
      readline.moveCursor(process.stdout, -1, 0)
    }
  } catch (e) {
    process.stdout.write(`\x1b[31m${ e instanceof Error ? e.message : e }\x1b[0m`)
    if (process.env['APP_DEBUG']) process.stdout.write(`\x1b[33m${ e instanceof Error && e.stack ? e.stack : 'No info'}\x1b[0m`)
  }
})()
