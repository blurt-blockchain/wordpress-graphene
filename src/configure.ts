/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Require */
import inquirer, { Answers } from 'inquirer'
import Chance from 'chance'

/** Blockchain */
import { Client, PrivateKey, cryptoUtils } from '@beblurt/dblurt'

/** libs */
import { configGet, configSave, checkBlockchainAccount } from './libs/utils.js'

/** Modules */
import { readFooterFile } from './modules/feed/helpers/feed.js'

/** Helpers */
import { CHAIN, CONFIG } from './helpers/config.js'
const BLURT_TESTNET_DEFAULT: CHAIN = { // Default values
  ADDRESS_PREFIX:        'TST',
  CHAIN_ID:              '1df54a5cc86f7c7efee2402e1304df6eae24eb8766a63c0546c1b2511cf5eba6',
  NODES_RPC:             ['https://testnet-rpc.beblurt.com'],
  PUBLISHER_ACCOUNT:     '',
  PUBLISHER_POSTING_KEY: ''
}
const BLURT_MAIN_CHAIN_DEFAULT: CHAIN = { // Default values
  ADDRESS_PREFIX:        'BLT',
  CHAIN_ID:              'cd8d90f29ae273abec3eaa7731e25934c63eb654d55080caff2ebb7f5df6381f',
  NODES_RPC:             ['https://rpc.beblurt.com','https://rpc.blurt.world'],
  PUBLISHER_ACCOUNT:     '',
  PUBLISHER_POSTING_KEY: ''
}

let nameAccount: string = '' // trick for checking the posting key
/** Check with the blockchain the authenticity of the posting key */
const checkAccountPostingKey = async (posting: string, prefix: string, rpcs: string[]): Promise<true|'Invalid account Posting key!'> => {
  try {
    const privatePosting = PrivateKey.from(posting)
    const publicPosting  = privatePosting.createPublic(prefix).toString()

    const client = new Client(rpcs)
    const account = await client.condenser.getAccounts([nameAccount])

    if(account[0]) {
      const key = account[0].posting.key_auths[0]? account[0].posting.key_auths[0][0] : null
      return publicPosting === key ? true : 'Invalid account Posting key!'
    } else {
      return 'Invalid account Posting key!'
    }
  } catch (e) {
    return 'Invalid account Posting key!'
  }
}

/** Generate random hash token for the HTTP communication with backend */
const generateToken = (): string => {
  const chance = new Chance()
  const token  = chance.hash({ length: 64 })
  console.log('\x1b[1;33m%s\x1b[0m', `Authentication Token (HTTP header):`, token)
  return token
}

/** Start the configuration */
(async (): Promise<void> => {
  try {
    /** Is there a previous configuration? */
    const previousConfig = await configGet()

    /** Common interactive command line user interfaces */
    const answers: Answers = await inquirer.prompt([
      /**
       * BLURT TESTNET
       **/
      {
        type: 'confirm',
        name: 'BLURT_TESTNET',
        message: 'There is a config for the BLURT TESTNET?',
        default: true
      },
      {
        type: 'input',
        name: 'BLURT_TESTNET_ADDRESS_PREFIX',
        message: 'BLURT TESTNET ADDRESS PREFIX',
        default: previousConfig?.BLURT.TESTNET?.ADDRESS_PREFIX ?? BLURT_TESTNET_DEFAULT.ADDRESS_PREFIX,
        when: (answers) => answers.BLURT_TESTNET,
      },
      {
        type: 'input',
        name: 'BLURT_TESTNET_CHAIN_ID',
        message: 'BLURT TESTNET CHAIN ID',
        default: previousConfig?.BLURT.TESTNET?.CHAIN_ID ?? BLURT_TESTNET_DEFAULT.CHAIN_ID,
        when: (answers) => answers.BLURT_TESTNET,
      },
      {
        type: 'input',
        name: 'BLURT_TESTNET_NODES_RPC',
        message: 'BLURT TESTNET NODES RPC (separated by comma if many)',
        default: previousConfig?.BLURT.TESTNET?.NODES_RPC.toString() ?? BLURT_TESTNET_DEFAULT.NODES_RPC.toString(),
        when: (answers) => answers.BLURT_TESTNET,
      },
      {
        type: 'input',
        name: 'BLURT_TESTNET_PUBLISHER_ACCOUNT',
        message: 'BLURT TESTNET PUBLISHER ACCOUNT',
        default: previousConfig?.BLURT.TESTNET?.PUBLISHER_ACCOUNT,
        when: (answers) => answers.BLURT_TESTNET,
        validate: async (value) => {
          const r = new RegExp(cryptoUtils.regExpAccount)
          if (r.test(value)) {
            nameAccount = await checkBlockchainAccount(value, BLURT_TESTNET_DEFAULT.NODES_RPC) ? value : ''
            return value === nameAccount ? true : 'Not exist on Blockchain!'
          } else {
            return 'Invalid account!'
          }
        },
      },
      {
        type: 'password',
        name: 'BLURT_TESTNET_PUBLISHER_POSTING_KEY',
        message: 'BLURT TESTNET PUBLISHER ACCOUNT POSTING KEY',
        default: previousConfig?.BLURT.TESTNET?.PUBLISHER_POSTING_KEY,
        when: (answers) => answers.BLURT_TESTNET,
        validate: async (value) => {
          return await checkAccountPostingKey(value, 'TST', BLURT_TESTNET_DEFAULT.NODES_RPC)
        },
      },

      /**
       * BLURT PROD
       **/
      {
        type: 'confirm',
        name: 'BLURT_MAIN_CHAIN',
        message: 'There is a config for the BLURT MAIN CHAIN?',
        default: true
      },
      {
        type: 'input',
        name: 'BLURT_MAIN_CHAIN_ADDRESS_PREFIX',
        message: 'BLURT MAIN CHAIN ADDRESS PREFIX',
        default: previousConfig?.BLURT.MAIN_CHAIN?.ADDRESS_PREFIX ?? BLURT_MAIN_CHAIN_DEFAULT.ADDRESS_PREFIX,
        when: (answers) => answers.BLURT_MAIN_CHAIN,
      },
      {
        type: 'input',
        name: 'BLURT_MAIN_CHAIN_ID',
        message: 'BLURT MAIN CHAIN ID',
        default: previousConfig?.BLURT.MAIN_CHAIN?.CHAIN_ID ?? BLURT_MAIN_CHAIN_DEFAULT.CHAIN_ID,
        when: (answers) => answers.BLURT_MAIN_CHAIN,
      },
      {
        type: 'input',
        name: 'BLURT_MAIN_CHAIN_NODES_RPC',
        message: 'BLURT MAIN CHAIN NODES RPC (separated by comma if many)',
        default: previousConfig?.BLURT.MAIN_CHAIN?.NODES_RPC.toString() ?? BLURT_MAIN_CHAIN_DEFAULT.NODES_RPC.toString(),
        when: (answers) => answers.BLURT_MAIN_CHAIN,
      },
      {
        type: 'input',
        name: 'BLURT_MAIN_CHAIN_PUBLISHER_ACCOUNT',
        message: 'BLURT MAIN CHAIN PUBLISHER ACCOUNT',
        default: previousConfig?.BLURT.MAIN_CHAIN?.PUBLISHER_ACCOUNT,
        when: (answers) => answers.BLURT_MAIN_CHAIN,
        validate: async (value) => {
          const r = new RegExp(cryptoUtils.regExpAccount)
          if (r.test(value)) {
            nameAccount = await checkBlockchainAccount(value, BLURT_MAIN_CHAIN_DEFAULT.NODES_RPC) ? value : ''
            return value === nameAccount ? true : 'Not exist on Blockchain!'
          } else {
            return 'Invalid account!'
          }
        },
      },
      {
        type: 'password',
        name: 'BLURT_MAIN_CHAIN_PUBLISHER_POSTING_KEY',
        message: 'BLURT MAIN CHAIN PUBLISHER ACCOUNT POSTING KEY',
        default: previousConfig?.BLURT.MAIN_CHAIN?.PUBLISHER_POSTING_KEY,
        when: (answers) => answers.BLURT_MAIN_CHAIN,
        validate: async (value) => {
          return await checkAccountPostingKey(value, 'BLT', BLURT_MAIN_CHAIN_DEFAULT.NODES_RPC)
        },
      },
      /**
       * HTTP
       **/
      {
        type: 'input',
        name: 'HTTP_PORT',
        message: 'What PORT to use for HTTP?',
        default: previousConfig?.HTTP.PORT ?? 3000,
        validate(value) {
          const r = new RegExp(/^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$/)
          return r.test(value) ? true : 'Invalid TCP PORT!'
        },
      },
      {
        type: 'confirm',
        name: 'HTTP_GENERATE_TOKEN',
        message: 'Generate an Authentication Token for HTTP Header Request?',
        default: true
      },
      {
        type: 'input',
        name: 'HTTP_TOKEN',
        message: 'Authentication Token for HTTP Header Request',
        default: previousConfig?.HTTP.TOKEN,
        when: (answers) => !answers.HTTP_GENERATE_TOKEN,
      },
    ])

    /** Authentication Token */
    const AUTHENTICATION_TOKEN = answers['HTTP_TOKEN'] ?? generateToken()

    /** START CONFIG */
    const config: CONFIG = {
      BLURT: {
        TESTNET: null,
        MAIN_CHAIN: null
      },
      HTTP: {
        PORT:  answers['HTTP_PORT'],
        TOKEN: AUTHENTICATION_TOKEN
      }
    }

    /** BLURT TESTNET */
    if (answers['BLURT_TESTNET']) {
      config.BLURT.TESTNET = {
        ADDRESS_PREFIX:        answers['BLURT_TESTNET_ADDRESS_PREFIX'],
        CHAIN_ID:              answers['BLURT_TESTNET_CHAIN_ID'],
        NODES_RPC:            (answers['BLURT_TESTNET_NODES_RPC']).trim().split(','),
        PUBLISHER_ACCOUNT:     answers['BLURT_TESTNET_PUBLISHER_ACCOUNT'],
        PUBLISHER_POSTING_KEY: answers['BLURT_TESTNET_PUBLISHER_POSTING_KEY']
      }
    }

    /** BLURT MAIN CHAIN */
    if (answers['BLURT_MAIN_CHAIN']) {
      config.BLURT.MAIN_CHAIN = {
        ADDRESS_PREFIX:        answers['BLURT_MAIN_CHAIN_ADDRESS_PREFIX'],
        CHAIN_ID:              answers['BLURT_MAIN_CHAIN_ID'],
        NODES_RPC:            (answers['BLURT_MAIN_CHAIN_NODES_RPC']).trim().split(','),
        PUBLISHER_ACCOUNT:     answers['BLURT_MAIN_CHAIN_PUBLISHER_ACCOUNT'],
        PUBLISHER_POSTING_KEY: answers['BLURT_MAIN_CHAIN_PUBLISHER_POSTING_KEY']
      }
    }

    await configSave(config)
    await readFooterFile()
    console.log('\x1b[\x1b[1;36m%s\x1b[0m', 'Settings saved successfully!')
  } catch (e) {
    if (e instanceof Error) {
      console.log('\x1b[31m%s\x1b[0m', e.message)
      console.log('\x1b[33m%s\x1b[0m', e.stack)
    } else {
      console.log('\x1b[31m%s\x1b[0m', e)
    }
  }
})()
