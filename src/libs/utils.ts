/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Filesystem */
import fs from 'fs'
const { readFile, writeFile } = fs.promises
interface ErrnoException extends Error {
  errno?:   number | undefined
  code?:    string | undefined
  path?:    string | undefined
  syscall?: string | undefined
}

/** Blockchain */
import { Client } from '@beblurt/dblurt'

/** Helpers */
import { CONFIG, checkConfig } from '../helpers/config.js'

export const jsonParse = (txt: string): {[key: string]: any} => {
  try {
    return JSON.parse(txt)
  } catch (e) {
    console.log('\x1b[31m%s\x1b[0m', `Error: ${ e instanceof Error ? e.message : e }`)
    console.log('\x1b[33m%s\x1b[0m', 'Stack:', e instanceof Error && e.stack ? e.stack : 'No info')
    return {}
  }
}

export const configGet = async (): Promise<CONFIG|undefined> => {
  try {
    const bufferConfig = await readFile(new URL('../../config.json', import.meta.url))
    const config  = jsonParse(bufferConfig.toString()) as CONFIG
    await checkConfig(config)
    return config
  } catch (e) {
    if(e instanceof Error && (e as ErrnoException).code === 'ENOENT') {
      return undefined
    } else {
      throw e
    }
  }
}

export const configSave = async (config: CONFIG): Promise<CONFIG> => {
  try {
    await checkConfig(config)
    await writeFile(new URL('../../config.json', import.meta.url), JSON.stringify(config))
    return config
  } catch (e) {
    throw e
  }
}

export const checkBlockchainAccount = async (name: string, rpcs: string[]): Promise<boolean> => {
  try {
    const client = new Client(rpcs)
    const account = await client.condenser.getAccounts([name])
    return account[0]?.name === name ? true : false
  } catch (e) {
    throw e
  }
}
