/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { createLogger, format, transports } from 'winston'
const  { combine, timestamp, colorize, printf } = format
import 'winston-daily-rotate-file'

/** Define log file */
const logFormat = printf(({ level, message, label, block , timestamp }) => {
  const msg = message ? message.replace('/;/g','.,') : 'Unknown error'
  return `${timestamp};${level};[${label}];${block ?? null};${msg}`
})

const logTransport = new transports.DailyRotateFile({
  filename: './logs/WINSTON-%DATE%.log',
  datePattern: 'YYYY-MM-DD-HH',
  frequency: '3h',
  zippedArchive: true,
  maxSize: '20m',
  maxFiles: '1d',
  format: combine(
    logFormat,
  ),
  level: 'info'
})

/** Logger */
const logger = createLogger({
  format: combine(
    timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
  ),
  transports: [
    //logErrorTransport,
    logTransport,
    // new transports.Console({
    //   format: combine(
    //     colorize(),
    //     logFormat
    //   ),
    // }),
  ]
})

export {
  logger
}