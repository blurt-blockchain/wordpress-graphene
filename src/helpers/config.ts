/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import Joi from 'joi'

/** Blockchain */
import { cryptoUtils } from '@beblurt/dblurt'

export const RegExpEnv = /^TESTNET|MAIN_CHAIN/
export type  ENV       = 'TESTNET'|'MAIN_CHAIN'

export interface CHAIN {
  ADDRESS_PREFIX:        string        // ADDRESS PREFIX
  CHAIN_ID:              string        // CHAIN ID
  NODES_RPC:             string[]      // List of RPC nodes
  PUBLISHER_ACCOUNT:     string        // BLURT ACCOUNT
  PUBLISHER_POSTING_KEY: string        // POSTING KEY OF BLURT ACCOUNT
}

export interface CONFIG {
  BLURT: {
    TESTNET:    CHAIN|null
    MAIN_CHAIN: CHAIN|null
  },
  HTTP: {
    PORT:  number
    TOKEN: string // For secure API
  }
}

const schemaChain = Joi.object({
  ADDRESS_PREFIX:        Joi.string().required(),
  CHAIN_ID:              Joi.string().required(),
  NODES_RPC:             Joi.array().items(Joi.string()).required(),
  PUBLISHER_ACCOUNT:     Joi.string().regex(cryptoUtils.regExpAccount).required(),
  PUBLISHER_POSTING_KEY: Joi.string().required(),
})

const schemaConfig = Joi.object({
  BLURT: Joi.object({
    TESTNET:    Joi.alternatives().try(schemaChain, Joi.allow(null)),
    MAIN_CHAIN: Joi.alternatives().try(schemaChain, Joi.allow(null)),
  }),
  HTTP: Joi.object({
    PORT:  Joi.number().integer().min(1).max(65535).required(),
    TOKEN: Joi.string().required(),
  }),
})

export const checkConfig = async (source: CONFIG): Promise<CONFIG> => {
  try {
    return await schemaConfig.validateAsync(source)
  } catch (e) {
    throw e
  }
}
