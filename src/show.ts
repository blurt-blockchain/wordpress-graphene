/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'

import { createRequire } from "module"
const require            = createRequire(import.meta.url)
const packageJson        = require('../package.json')
process.env['VERSION']   = packageJson.version

/** Utils */
import { jsonParse } from './libs/utils.js'

/** Filesystem */
import fs from 'fs'
const { readFile } = fs.promises

/** Commander */
import { Command } from 'commander'
const program = new Command()
program
  .version(packageJson.version)
  .option('-f,      --file <FILE>',   '<name file>.json to output')
  .option('-d,      --debug',         'output extra debugging');

/** Start the Application */
(async function(): Promise<void> {
  try {
    program.parse(process.argv)
    const options = program.opts()
    assert(options['file'], 'No file defined')

    /** posts path */
    const dataPath = '../data/posts'
    const f = await readFile(new URL(`${ dataPath }/${ options['file'] }`, import.meta.url))
    const ops = jsonParse(f.toString())

    console.log(ops['commentOperation'][1].body)
  } catch (e) {
    console.log(e)
  }
})()