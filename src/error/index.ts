/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
export class errorServer extends Error {
  code:  number | undefined
  data: {
    translate: string
  }

  constructor (code: number, message: string, data: {translate: string}) {
    super(message)

    // capturing the stack trace keeps the reference to your error class
    Error.captureStackTrace(this, this.constructor);

    // assign the error class name in your custom error (as a shortcut)
    this.name = this.constructor.name

    // you may also assign additional properties to your error
    this.code     = code ? code : -32601
    this.message  = message ? message : 'Server error'
    this.data     = data ? data : { translate: 'MSG.ERR_SERVER' }
  }
}