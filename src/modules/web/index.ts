/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import dayjs from 'dayjs'

import http    from 'http'
import express from 'express'

/** loaders */
import expressLoader from './express.js'

interface ErrnoException extends Error {
  errno?:   number
  code?:    string
  path?:    string
  syscall?: string
  stack?:   string
  msg?:     string
}

export class ExpressServer {
  port: number

  /** Port management */
  constructor(port: number){
    this.port = port
  }

  /** Starts the server */
  async start(): Promise<void> {
    try {
      const app = express()
      await expressLoader({ app })

      /** Create HTTP server */
      const server = http.createServer(app)

      /** Event listener for HTTPs server "listening" event. */
      const onListening = () => {
        const addr = server.address()
        const bind = typeof addr === 'string' ? addr : addr?.port
        
        console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => http expressServer => \x1b[0;32m%s\x1b[0m`, 'Server Listening', `=> \x1b[1;33m${ bind }\x1b[0m (port TCP)`)
      }

      /** Listen on provided port, on all network interfaces. */
      server.listen(this.port)
      server.on('error', this.onError)
      server.on('listening', onListening)
    } catch (e) {
      this.onError(e instanceof Error ? e : e as any)
    }
  }

  /** Error management */
  onError(e: ErrnoException): void {
    if (e.syscall !== 'listen') {
      throw e
    }

    const bind = typeof this.port === 'string' ?
    'Pipe ' + this.port :
    'Port ' + this.port

    /** Handle specific listen errors with friendly messages */
    switch (e.code) {
    case 'EACCES':
      console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => http expressServer => \x1b[31m%s\x1b[0m`, bind + ' requires elevated privileges')
      process.exit(1)
      
    case 'EADDRINUSE':
      console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => http expressServer => \x1b[31m%s\x1b[0m`, bind + ' is already in use')
      process.exit(1)
      
    default:
      console.log('\x1b[31m%s\x1b[0m', `Error: ${e.message}`)
      if(process.env['APP_DEBUG']) console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => http expressServer => \x1b[33m%s\x1b[0m`, 'Stack:', e.stack ? e.stack : 'No info')
      process.exit(1)
      
    }
  }
}