/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'
import { errorServer } from '../../error/index.js'
import express, { Request, Response } from 'express'

/* libs */
import { logger } from '../../libs/logger.js'
import { configGet } from '../../libs/utils.js'

/** Validator */
import { ismethodValid } from "./helpers/index.js"

/** JsonRPC */
import { feed } from "../feed/json-rpc/index.js"

export default async ({ app }: { app: express.Application }): Promise<express.Application> => {
  const config = await configGet()
  assert(config, 'No configuration present!')

  app.enable('trust proxy')

  app.get('/rpc/status', (_req: Request, res: Response) => { res.status(200).end() })
  app.head('/rpc/status', (_req: Request, res: Response) => { res.status(200).end() })

  /** Ping to check if the nodejs Application is alive */
  app.get(
    '/rpc/ping', (_req: Request, res: Response) => res.sendStatus(200)
  )

  /** JsonRPC */
  app.post(
    '/rpc',
    (req: Request, res: Response) => {
      try {
        /** Retrieve bearer */
        const bearer = req.headers.authorization ? req.headers.authorization.split(' ') : []
        if(bearer.length < 2) throw new errorServer(-32602, `Unknown bearer token`, { translate: `MSG.D_ERROR_RECEIVED` })
        if(bearer[1] !== config.HTTP.TOKEN) throw new errorServer(-32602, `Error wrong bearer: ${ bearer[1] }`, { translate: `MSG.D_ERROR_RECEIVED` })

        /** Body is present? */
        if(typeof req.body.id !== 'string' && typeof req.body.id !== 'number') throw new errorServer(-32602, `No data received!`, { translate: `MSG.ERR_NO_DATA_RECEIVED` })

        /** Method valid? */
        if (!req.body.method || !ismethodValid(req.body.method)) throw new errorServer(-32603, `Method error`, { translate: `MSG.ERR_JSONRPC_METHOD` })

        const method = req.body.method.split('.')
        switch (method[0]) {
          /** Feed */
          case 'feed':
            feed(req, res)
            break

          default:
            res.json({ jsonrpc: '2.0', error: { code: -32601, message: 'Method not found', data: {} }, id: req.body.id })
            break
        }
      } catch(err) {
        const id = req.body && req.body.id ? req.body.id : null
        const e: errorServer = err instanceof errorServer ? err : new errorServer(-32602, err instanceof Error ? err.message : 'unknown JsonRPC error!', { translate: `MSG.ERR_SERVER` })
        logger.log({ level: 'error', label: 'JSONRPC', message: e instanceof Error ? e.message : `unknown JsonRPC error!` })
        if (process.env['APP_DEBUG']) {
          console.log('\x1b[31m%s\x1b[0m', 'Error', e.message)
          console.log('\x1b[33m%s\x1b[0m', 'Stack:', e.stack ? e.stack : 'No info')
        }
        if(e.code && e.code === -32602) {
          res.json({ jsonrpc: '2.0', error: { code: e.code, message: e.message, data: e.data ? Object.assign({ message: e.message }, e.data) : { message: e.message } }, id })
        } else if(e.code && e.code === -32603){
          res.json({ jsonrpc: '2.0', error: { code: e.code, message: 'Internal error', data: e.data ? Object.assign({ message: e.message }, e.data) : { message: e.message } }, id })
        } else {
          res.json({ jsonrpc: '2.0', error: { code: -32600, message: 'Invalid Request', data: { message: e.message } }, id })
        }
      }
    }
  )
  
  /** Return the express app */
  return app
}