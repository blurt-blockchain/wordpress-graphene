/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import express, { Request, Response } from 'express'
import expressRouting from './express.routing.js'

import path from 'path'
import { fileURLToPath } from 'url'
const __filename = fileURLToPath(import.meta.url)
const __dirname  = path.dirname(__filename)

import cookieParser   from 'cookie-parser'
import helmet         from 'helmet'
import methodOverride from 'method-override'

import cors from 'cors'
// const corsOptions = {
//   origin: "*",
//   methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
//   preflightContinue: false,
//   optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
// }

import dayjs from 'dayjs'

export default async ({ app }: { app: express.Application }): Promise<express.Application> => {
  /** disable X-Powered-By header */
  app.disable("x-powered-by")

  /** secure by setting various HTTP headers => https://github.com/helmetjs/helmet */
  app.use(helmet())
  app.use(helmet.xssFilter())
  // Set Content Security Policies
  // const scriptSources = ["'self'", "'unsafe-inline'", "'unsafe-eval'"]
  app.use(
    helmet.contentSecurityPolicy({
      useDefaults: true,
      directives: {
        "script-src": ["'self'", "http://localhost", "'unsafe-inline'", "'unsafe-eval'"],
        "img-src": ["'self'", "http://localhost", "'unsafe-inline'", "'unsafe-eval'"],
        "connect-src": ["'self'", "http://localhost", "'unsafe-inline'", "'unsafe-eval'"],
      }
    })
  )

  /** Body parsing middleware */
  app.use(express.json({
    inflate: true,
    limit: '100kb',
    // reviver: null,
    strict: true,
    type: 'application/json',
    verify: undefined,
  }))

  app.use(express.urlencoded({
    extended: true,
    inflate: true,
    limit: '5kb',
    parameterLimit: 1000,
    type: 'application/x-www-form-urlencoded',
    verify: undefined,
  }))

  /** Cookie parser middleware. => https://github.com/expressjs/cookie-parser */
  app.use(cookieParser())

  /** enable CORS middleware (include before other routes) => https://github.com/expressjs/cors */
  // app.use(cors(corsOptions))
  app.use(cors())

  /** Override HTTP verbs. => https://github.com/expressjs/method-override */
  app.use(methodOverride())

  /** Define ejs as view engine */
  app.set('views', path.join(__dirname, '../../../views'))
  app.set('view engine', 'ejs')
 
  /**
   * ROUTING MANAGEMENT
   */
   await expressRouting({ app })

  /** CATCH 404 and forward to error handler */
  app.use( (req: Request, res: Response) => {
    console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | \x1b[33m%s\x1b[0m`, req.url, `Not Found!`)
    console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | \x1b[33m%s\x1b[0m`,req.method, `Method`)
    console.log(req.headers)
    if (req.body) console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | \x1b[33m%s\x1b[0m`, 'body:', req.body)

    res.status(404);
    res.render('error', {
      status: 404,
      message: 'Page not found',
      error: {}
    })
  })

  app.use( (error: Error, req :Request, res :Response) => {
    console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | \x1b[33m%s\x1b[0m`, req.url, error.message)
    console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | \x1b[33m%s\x1b[0m`,req.method, `Method`)
    console.log(req.headers)
    if (req.body) console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | \x1b[33m%s\x1b[0m`, 'body:', req.body)

    res.status(500)
    res.render('error', {
      status: 500,
      message: 'Server error',
      error: {}
    })
  })

  // Return the express app
  return app
}