/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'

export const ismethodValid = async (value: string): Promise<boolean> => {
  try {
    assert(value !== undefined && value !== null && value.toString().length !== 0, `${value} can't be tested!`)
    let result = false
    const re = new RegExp(/^([a-z\_]{1,}\.[a-z\_]{1,})$/)
    result = re.test(value)
    if(result) {
      return true
    } else {
      return false
    } 
  } catch (e) {
    return false
  }
}