/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/** Require */
import axios  from 'axios'
import Parser from 'rss-parser'
import dayjs  from 'dayjs'

/** Html => Markdown */
import { NodeHtmlMarkdown } from 'node-html-markdown'
const nhm = new NodeHtmlMarkdown()

/** Libs */
import { logger } from '../../../libs/logger.js'

/** Helpers */
import { CHAIN, ENV } from '../../../helpers/config.js'
import { FEED, checkFile, saveFeedFile, API_WP_POST, API_WP_MEDIA, readFooterFile, savePostOperationFile } from '../helpers/feed.js'

/** dblurt */
import { Client, CommentOperation, CommentOptionsOperation, Operation, PrivateKey } from '@beblurt/dblurt'

let feeds: FEED[] = []

export class Feeds {
  config: CHAIN
  parseInterval?: NodeJS.Timeout

  constructor(config: CHAIN) {
    this.config = config
  }

  async load(): Promise<void> {
    feeds = await checkFile(process.env["ENV"] as ENV)
    console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => \x1b[0;32m%s\x1b[0m`, 'Loaded', `=> \x1b[1;33m${ feeds.length }\x1b[0m feeds`)
    return
  }

  /** permlink Generator */
  async permlinkGenerator(author: string, title: string): Promise<string> {
    const words = title.toLocaleLowerCase().match(/([a-z09]){0,}/g);
    if(words) {
      let permlink = '';
      for await (const w of words) {
        if(w && permlink.length <= 128) {
          permlink += permlink ? '-' + w : w;
        }
      }
      const client = new Client(this.config.NODES_RPC)
      const p = await client.condenser.getContent(author, permlink)
      if(p && p.body) {
        permlink += `-${Date.now()}`
      }
      return permlink;
    } else {
      throw new Error('permlink generator error');
    }
  }

  /** Read API and prepare ops */
  async readApi(url: string, feed?: FEED): Promise<{ ops?: Operation[], nbPost?: number, nbPostTreated?: number, status: 'failed'|'success' }> {
    try {
      const r     = await axios.get<API_WP_POST[]>(`${ url }/wp-json/wp/v2/posts`)
      const posts = r.data
      let nb = 0
      const ops: Operation[] = []

      if(feed) {
        /** For each post */
        for await (const post of posts) {
          /** Check date */
          const postDate      = dayjs(`${ post.date_gmt.replace('T', ' ') } +00:00`, 'YYYY-MM-DD HH:mm:ss Z')
          const feedLastBatch = dayjs(feed.last_batch)

          /** If date Ok add to the body of the post */
          if (postDate.diff(feedLastBatch, 'second') > 3) {
            nb++

            const title = post.title.rendered
            const tags  = [ feed.mainTag ]

            let cover: string|undefined
            if(post.featured_media > 0) {
              const featured_media = await axios.get<API_WP_MEDIA>(`${ feed.blogURL }/wp-json/wp/v2/media/${ post.featured_media }`)
              if (featured_media.data.guid.rendered) cover = featured_media.data.guid.rendered
            }

            let body = cover ? `[![${ title }](${ cover })](${ post.link })\n\n` : ``
            
            body += `## [${ title }](${ post.link })\n\n${ nhm.translate(post.content.rendered) }\nDiscover more on: ${ feed.blogURL }\n\n---\n\n`

            /** JSON METADATA */
            const json_metadata = {
              app:         `wordpress-graphene/${ process.env['VERSION'] }`,
              format:      'markdown',
              tags,
            }
    
            const author   = this.config.PUBLISHER_ACCOUNT
            const permlink = await this.permlinkGenerator(author, title)

            const commentOperation: CommentOperation = [
              'comment',
              {
                parent_author: '',
                parent_permlink: tags[0]!,
                author,
                permlink,
                title,
                body: `${ body } ${ await readFooterFile()}`,
                json_metadata: JSON.stringify(json_metadata)
              }
            ]
    
            const b: { account: string; weight: number }[] = feed.beneficiaries
            const beneficiaries: { account: string; weight: number }[] = b.sort((a, b) => a.account < b.account ? -1 : 1)
    
            const commentOptionsOperation: CommentOptionsOperation = [
              'comment_options',
              {
                author,
                permlink,
                allow_curation_rewards: true,
                allow_votes:            true,
                max_accepted_payout:    process.env["ENV"] as ENV === 'TESTNET' ? '1000000.000 TESTS' : '1000000.000 BLURT',
                extensions: [[0, { beneficiaries }]]
              }
            ]

            ops.push(commentOperation, commentOptionsOperation)
            await savePostOperationFile(`API_${permlink}`, { commentOperation, commentOptionsOperation})
          }
        }
      }
  
      return { ops, nbPost: posts.length, nbPostTreated: nb, status: 'success' }
    } catch (e) { 
      if (process.env['APP_DEBUG']) {
        console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => API => \x1b[31m${ e instanceof Error ? e.message : e }\x1b[0m`)
        // console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => API => \x1b[33m${ 'Stack:' }\x1b[0m ${ e instanceof Error && e.stack ? e.stack : 'No info' }`)
      }
      return { status: 'failed' }
    }
  }

  /** Read RSS and prepare ops */
  async readRss(url: string, feed?: FEED): Promise<{ blog?: string, ops?: Operation[], nbPost?: number, nbPostTreated?: number, status: 'failed'|'success' }> {
    try {
      const parser = new Parser({
        timeout: 60000,
      })
      const blog = await parser.parseURL(`${ url }/rss`)
      let nb = 0
      const ops: Operation[] = []
  
      if(feed) {
        for await (const post of blog.items) {
          /** Check date */
          const postDate      = dayjs(`${ post.isoDate!.replace('T', ' ') } +00:00`, 'YYYY-MM-DD HH:mm:ss Z')
          const feedLastBatch = dayjs(feed.last_batch)

          /** If date Ok add to the body of the post */
          if (postDate.diff(feedLastBatch, 'second') > 3) {
            nb++

            const title = post.title
            const tags  = [ feed.mainTag ]

            const body = `## [${ title }](${ post.link })\n\n${ post.content ? post.content : '' }\n\n---\n\n`

            /** JSON METADATA */
            const json_metadata = {
              app:         `wordpress-graphene/${ process.env['VERSION'] }`,
              format:      'markdown',
              tags,
            }
    
            const author   = this.config.PUBLISHER_ACCOUNT
            const permlink = await this.permlinkGenerator(author, title ?? Date.now().toString())

            const commentOperation: CommentOperation = [
              'comment',
              {
                parent_author: '',
                parent_permlink: tags[0]!,
                author,
                permlink,
                title: title ?? Date.now().toString(),
                body: `${ body } ${ await readFooterFile()}`,
                json_metadata: JSON.stringify(json_metadata)
              }
            ]
    
            const b: { account: string; weight: number }[] = feed.beneficiaries
            const beneficiaries: { account: string; weight: number }[] = b.sort((a, b) => a.account < b.account ? -1 : 1)
    
            const commentOptionsOperation: CommentOptionsOperation = [
              'comment_options',
              {
                author,
                permlink,
                allow_curation_rewards: true,
                allow_votes:            true,
                max_accepted_payout:    process.env["ENV"] as ENV === 'TESTNET' ? '1000000.000 TESTS' : '1000000.000 BLURT',
                extensions: [[0, { beneficiaries }]]
              }
            ]

            ops.push(commentOperation, commentOptionsOperation)
            await savePostOperationFile(`RSS_${permlink}`, { commentOperation, commentOptionsOperation})
          }
        }
      }
  
      return { blog: blog.title, ops, nbPost: blog.items.length, nbPostTreated: nb, status: 'success' }
    } catch (e) { 
      if (process.env['APP_DEBUG']) {
        console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => RSS => \x1b[31m${ e instanceof Error ? e.message : e }\x1b[0m`)
        // console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => RSS => \x1b[33m${ 'Stack:' }\x1b[0m ${ e instanceof Error && e.stack ? e.stack : 'No info' }`)
      }
      return { ops: [], nbPost: 0, nbPostTreated: 0, status: 'failed' }
    }
  }

  /** Add a feed */
  async addFeed(feed: FEED): Promise<string> {
    try {
      const blogURL = feeds.find(f => f.blogURL === feed.blogURL)
      const feed_id = feeds.find(f => f._id === feed._id)
      if(!blogURL && !feed_id) {
        const [api, rss] = await Promise.all([
          await this.readApi(feed.blogURL),
          await this.readRss(feed.blogURL)
        ])
        const size = api.nbPost ?? rss.nbPost
        const apiStatus = api.status === 'failed' ? '\x1b[1;31mFailed\x1b[0m' : '\x1b[1;32mOK\x1b[0m'
        const rssStatus = rss.status === 'failed' ? '\x1b[1;31mFailed\x1b[0m' : '\x1b[1;32mOK\x1b[0m'
  
        feeds.push(feed)
        await saveFeedFile(process.env["ENV"] as ENV, feeds)
  
        feeds = await checkFile(process.env["ENV"] as ENV)
  
        /** Show the feed added */
        console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => \x1b[0;32m${ feed.blogURL }\x1b[0m added (API: ${ apiStatus } / RSS: ${ rssStatus }) => \x1b[1;33m${ size }\x1b[0m posts`)
        console.log(feed)
        console.log()
  
        return `Feed: \x1b[1;33m${ size }\x1b[0m posts for \x1b[0;36m${ feed.blogURL }\x1b[0m => API: ${ apiStatus } / RSS: ${ rssStatus }`
      } else {
        throw new Error(`${ feed.blogURL } already in Feeds!`)
      }
    } catch (e) {
      logger.log({ level: 'error', label: 'addFeed', message: e instanceof Error ? e.message : 'Start error' })
      throw e
    }
  }

  async parse(): Promise<void> {
    try {
      for (const feed of feeds) {
        const [api, rss] = await Promise.all([
          await this.readApi(feed.blogURL, feed),
          await this.readRss(feed.blogURL, feed)
        ])

        if(api.status === 'success') {
          if(api.nbPostTreated && api.nbPostTreated > 0) console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => Parse \x1b[0;35m${ feed.blogURL }\x1b[0m => \x1b[0;32m${ 'API' }\x1b[0m => \x1b[0;33m${api.nbPostTreated}\x1b[0m / ${api.nbPost}`)

          // const posting = PrivateKey.from(this.config.PUBLISHER_POSTING_KEY)
          // const client = new Client(this.config.NODES_RPC, { addressPrefix: this.config.ADDRESS_PREFIX, chainId: this.config.CHAIN_ID })
          // const r = await client.broadcast.sendOperations([comment, commentOptionsOperation], posting)
          // console.log(r)

        } else if(rss.status === 'success') {
          if(rss.nbPostTreated && rss.nbPostTreated > 0) console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => Parse \x1b[0;35m${ feed.blogURL }\x1b[0m => \x1b[0;33m${ 'RSS' }\x1b[0m => \x1b[0;33m${rss.nbPostTreated}\x1b[0m / ${rss.nbPost}`)

          // const posting = PrivateKey.from(this.config.PUBLISHER_POSTING_KEY)
          // const client = new Client(this.config.NODES_RPC, { addressPrefix: this.config.ADDRESS_PREFIX, chainId: this.config.CHAIN_ID })
          // const r = await client.broadcast.sendOperations([comment, commentOptionsOperation], posting)
          // console.log(r) 

        }

        feed.last_batch = (new Date()).toISOString()
        await saveFeedFile(process.env["ENV"] as ENV, feeds)
      }
    } catch (e) {
      throw e
    }
  }

  async start(): Promise<void> {
    try {
      await this.load()
      this.parseInterval = setInterval(async () => {
        await this.parse()
      }, 180000);
    } catch (e) {
      /** Clear interval */
      if(this.parseInterval) { clearInterval(this.parseInterval)}
      /** Log the error */
      if (process.env['APP_DEBUG']) {
        console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => Start => \x1b[31m${ 'Error:' }\x1b[0m ${ e instanceof Error ? e.message : e }`)
        console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Module => Feeds => Start => \x1b[33m${ 'Stack:' }\x1b[0m ${ e instanceof Error && e.stack ? e.stack : 'No info' }`)
      }
      logger.log({ level: 'error', label: 'Feeds', message: e instanceof Error ? e.message : 'Start error' })
      /** Re start the process */
      await this.start()
    }
  }
}