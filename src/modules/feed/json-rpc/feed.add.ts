/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'

/** Require */
import { errorServer } from '../../../error/index.js'
import { logger }      from '../../../libs/logger.js'
import { Request, Response } from 'express'

/** Helpers */
import { FEED, isFeedValid } from '../helpers/feed.js'

/** Modules */
import { Feeds } from '../module/feed.js'

/** Libs */
import { configGet } from '../../../libs/utils.js'
import { ENV } from '../../../helpers/config.js'

export const feedAdd = async (req: Request, res: Response): Promise<void> => {
  try {
    const body = req.body
    assert(body.params?.feed, 'Missing feed')

    /** Add a Signature check (with CONFIG.ADMINS) */
    assert(body.params?.signature, 'Missing Signature')

    const feedValid = await isFeedValid(body.params?.feed as FEED)
    const config = await configGet()
    assert(config, `No Config available!`)
    const chain = process.env["ENV"] as ENV === 'MAIN_CHAIN' ? config?.BLURT.MAIN_CHAIN : config?.BLURT.TESTNET
    assert(chain, `No chain data!`)

    const feed = new Feeds(chain)
    const r = await feed.addFeed(feedValid)

    /** Send Result */
    res.json({ jsonrpc: '2.0', result: [r], id: body.id ? body.id : 0 })
  } catch(e) {
    console.log(e)
    const err = e as errorServer
    logger.log({ level: 'error', label: 'feedAdd', message: err.message })
    res.json({ jsonrpc: '2.0', error: { code: err.code, message: err.message, data: err.data }, id: req.body.id ? req.body.id : null })
  }
}