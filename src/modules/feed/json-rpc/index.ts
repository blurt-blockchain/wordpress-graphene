/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import { Request, Response } from 'express'

/** methods */
import { feedAdd } from './feed.add.js'

/** Helpers */
import { FeedMethods } from '../helpers/feed.js'

export const feed = async (req: Request, res: Response): Promise<void> => {
  switch (req.body.method as FeedMethods) {

    /** Add a feed */
    case 'feed.add':
      feedAdd(req, res)
    break

    default:
      res.json({ jsonrpc: '2.0', error: { code: -32601, message: 'Method not found', data: {} }, id: req.body.id })
      break
  }
}