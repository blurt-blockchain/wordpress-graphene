/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import Joi from 'joi'

/** dblurt */
import { cryptoUtils } from '@beblurt/dblurt'

/** Utils */
import { jsonParse } from '../../../libs/utils.js'

/** Helpers */
import { ENV } from '../../../helpers/config.js'

/** Filesystem */
import fs from 'fs'
const { stat, mkdir, readFile, writeFile } = fs.promises
interface ErrnoException extends Error {
  errno?:   number | undefined
  code?:    string | undefined
  path?:    string | undefined
  syscall?: string | undefined
}

export const RegExpUrl = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:\/?#[\]@!\$&'\(\)\*\+,=.]+$/

/** Methods */
export type FeedMethods = "feed.add"

/** Interfaces */
export interface BENEFICIARY {
  account: string
  weight:   number
}

export interface FEED {
  _id:             string
  feedType:        'RSS'|'API'
  blogURL:         string
  mainTag:         string
  verificationURL: string
  beneficiaries:   BENEFICIARY[]
  last_batch:      string
}

const schemaBeneficiary = Joi.object({
  account: Joi.string().regex(cryptoUtils.regExpAccount).required(),
  weight:  Joi.number().integer().min(1).max(10000).required(),
})

const schemaFeed = Joi.object({
  _id:             Joi.string().required(),
  feedType:        Joi.string().regex(/^RSS|API$/).required(),
  blogURL:         Joi.string().regex(RegExpUrl).required(),
  mainTag:         Joi.string().regex(/^\w+$/).required(),
  verificationURL: Joi.string().regex(RegExpUrl).required(),
  beneficiaries:   Joi.array().items(schemaBeneficiary).required(),
  last_batch:      Joi.string().required(),
})

export const isFeedValid = async (source: FEED): Promise<FEED> => {
  try {
    return await schemaFeed.validateAsync(source) as FEED
  } catch (e) {
    throw e
  }
}

/** data path */
const dataPath = '../../../../data'

const checkPath = async (folder: string): Promise<void> => {
  return new Promise((resolve, reject) => {
    stat(new URL(folder, import.meta.url))
      .then(() => resolve())
      .catch(async e => {
        if(e instanceof Error && (e as ErrnoException).code === 'ENOENT') {
          mkdir(new URL(folder, import.meta.url), { recursive: true })
            .then(() => resolve())
            .catch(e => reject(e))
        } else {
          reject(e)
        }
      })
  })
}

export const checkFile = async (env: ENV): Promise<FEED[]> => {
  return new Promise((resolve, reject) => {
    checkPath(dataPath)
      .then(() => {
        stat(new URL(`${ dataPath }/${ env }_FEEDS.json`, import.meta.url))
          .then(async () => {
            const b = await readFile(new URL(`${ dataPath }/${ env }_FEEDS.json`, import.meta.url))
            resolve(jsonParse(b.toString()) as FEED[])
          })
          .catch(async e => {
            if(e instanceof Error && (e as ErrnoException).code === 'ENOENT') {
              writeFile(new URL(`${ dataPath }/${ env }_FEEDS.json`, import.meta.url), JSON.stringify([]))
                .then(() => resolve([]))
                .catch(e => reject(e))
            } else {
              reject(e)
            }
          })
      })
      .catch(e => reject(e))
  })
}

export const saveFeedFile = async (env: ENV, feeds: FEED[]): Promise<void> => {
  try {
    await writeFile(new URL(`${ dataPath }/${ env }_FEEDS.json`, import.meta.url), JSON.stringify(feeds))
    return
  } catch (e) {
    throw e
  }
}

export const savePostOperationFile = async (file: string, ops: any): Promise<void> => {
  return new Promise((resolve, reject) => {
    checkPath(`${ dataPath }/posts`)
      .then(async () => {
        await writeFile(new URL(`${ dataPath }/posts/${ file }.json`, import.meta.url), JSON.stringify(ops))
        resolve()
      })
      .catch(e => reject(e))
  })
}

export interface API_WP_POST {
  id:       number
  date:     string // "2024-01-13T06:45:26"
  date_gmt: string
  guid: {
    rendered: string // "https://www.imtase.com/?p=5913"
  },
  modified:     string
  modified_gmt: string
  slug:         string // "my-graphene-test"
  status:       'publish'|string // "publish"
  type:         'post'|string // "post"
  link:         string // "https://www.imtase.com/my-graphene-test/"
  title: {
    rendered: string // "My graphene test"
  },
  content: {
      rendered:  string
      protected: boolean
  },
  excerpt: {
    rendered:  "<p>This is a test for a graphene post Let&#8217;s go!</p>\n",
    protected: false
  },
  author:         number // author id
  featured_media: number // cover id
  comment_status: 'open'|string
  ping_status:    'open'|string
  sticky:         boolean
  template:       string
  format:         'standard'|string
  meta:           any
  categories:     number[] // categories id
  tags:           number[] // tags id
  _links:         any
}

export interface API_WP_MEDIA {
  id:       number
  date:     string // "2024-01-13T06:45:26"
  date_gmt: string
  guid: {
    rendered: string // "https://www.imt-ase.com/wp-content/uploads/2017/01/wallpapers-linux-glass3-ubuntu.jpg"
  },
  modified:     string
  modified_gmt: string
  slug:         string
  status:       'publish'|'inherit'|string
  type:         'post'|'attachment'|string
  link:         string // "https://www.imt-ase.com/ubuntu-server-install-secure/wallpapers-linux-glass3-ubuntu/"
  title: {
    rendered: string
  },
  author:         number // author id
  comment_status: 'open'|string
  ping_status:    'open'|string
  template:       string
  meta:           any
  description:    {
    rendered: string
  }
  caption: {
    rendered: string
  }
  alt_text:   string
  media_type: 'image'|string
  mime_type:  'image/jpeg'|string
  media_details: {
    width:  number
    height: number
    file:   string
    sizes: any
    image_meta: any
  }
}

const footer = `\n[![Web 2.0 Support](https://img.blurt.world/blurtimage/mariuszkarowski/09dcb89216483eb183a0d385cefe7d0fa50ab4ce.png)](https://web2.support)
 
### Monetize your online content

Connect your website, blog, video channel, or any social media profile to the web 3.0 blockchain. You will receive free quality backlinks, new followers, and BLURT tokens, which you can convert to Bitcoin and fiat money. For more information, visit: https://web2.support`

export const readFooterFile = async (): Promise<string> => {
  return new Promise((resolve, reject) => {
    checkPath(dataPath)
      .then(() => {
        stat(new URL(`${ dataPath }/POST_FOOTER.md`, import.meta.url))
          .then(async () => {
            const f = await readFile(new URL(`${ dataPath }/POST_FOOTER.md`, import.meta.url))
            resolve(f.toString())
          })
          .catch(async e => {
            if(e instanceof Error && (e as ErrnoException).code === 'ENOENT') {
              writeFile(new URL(`${ dataPath }/POST_FOOTER.md`, import.meta.url), footer)
                .then(() => resolve(footer))
                .catch(e => reject(e))
            } else {
              reject(e)
            }
          })
      })
      .catch(e => reject(e))
  })
}