/** 
 * @project wordpress-graphene
 * @author  @nalexadre (https://beblurt.com/@nalexadre)
 * @license
 * Copyright (C) 2024  IMT ASE Co., LTD
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import assert from 'assert'
import dayjs from 'dayjs'

import { createRequire } from "module"
const require            = createRequire(import.meta.url)
const packageJson        = require('../package.json')
process.env['VERSION']   = packageJson.version

/** Libs */
import { logger } from './libs/logger.js'
import { configGet } from './libs/utils.js'

/** Helpers */
import { RegExpEnv } from './helpers/config.js'

/** Modules */
import { ExpressServer } from './modules/web/index.js'
import { Feeds }         from './modules/feed/module/feed.js'

/** Commander */
import { Command } from 'commander'
const program = new Command()
program
  .version(packageJson.version)
  .option('-env,    --environment <ENVIRONMENT>',   'Environment: TESTNET | MAIN_CHAIN', 'MAIN_CHAIN')
  .option('-d,      --debug',                       'output extra debugging');

/** Start the Application */
(async function(): Promise<void> {
  try {
    console.log(`\x1b[0;32m${ 'Wordpress Graphene' }\x1b[0m version \x1b[0;33m${ packageJson.version }\x1b[0m`)
    console.log()

    program.parse(process.argv)
    const options = program.opts()

    /** Environment */
    assert(options["environment"].match(RegExpEnv), `Environment not exist! Accept only => TESTNET | MAIN_CHAIN`)
    process.env["ENV"] = options["environment"]
    console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | Environment: ${ options["environment"] === 'TESTNET' ? '\x1b[0;33mTESTNET\x1b[0m' : '\x1b[0;35mMAIN CHAIN\x1b[0m' }`)

    /** Set Debug Mode if asked */
    if(options["debug"]) process.env["APP_DEBUG"] = 'DEBUG'

    /** Config */
    const config = await configGet()

    /** TESTNET */
    if(options["environment"] === 'TESTNET' && config?.BLURT.TESTNET) {
      /** Module => HTTP */
      const expressServer = new ExpressServer(config.HTTP.PORT)
      await expressServer.start()

      /** Module => Feeds */
      const feeds = new Feeds(config.BLURT.TESTNET)
      await feeds.start()
    } else if(options["environment"] === 'TESTNET' && !config?.BLURT.TESTNET) {
      console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | \x1b[0;31mCONFIGURATION DATA MISSING!\x1b[0m => \x1b[0;33mTESTNET\x1b[0m`)
    }

    /** MAIN CHAIN */
    if(options["environment"] === 'MAIN_CHAIN' && config?.BLURT.MAIN_CHAIN) {
      /** Module => HTTP */
      const expressServer = new ExpressServer(config.HTTP.PORT)
      await expressServer.start()

      /** Module => Feeds */
      const feeds = new Feeds(config.BLURT.MAIN_CHAIN)
      await feeds.start()
    } else if(options["environment"] === 'MAIN_CHAIN' && !config?.BLURT.MAIN_CHAIN) {
      console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | \x1b[0;31mCONFIGURATION DATA MISSING!\x1b[0m => \x1b[0;35mMAIN CHAIN\x1b[0m`)
    }

  } catch (e) {
    if (process.env['APP_DEBUG']) {
      console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | `, '\x1b[31m%s\x1b[0m', `Error: ${ e instanceof Error ? e.message : e }`)
      console.log(`\x1b[0;36m${ dayjs().format('HH:mm:ss SSS Z[Z]') }\x1b[0m | `, '\x1b[33m%s\x1b[0m', 'Stack:', e instanceof Error && e.stack ? e.stack : 'No info')
    }
    logger.log({ level: 'error', label: 'start', message: e instanceof Error ? e.message : 'Start error' })
  }
})()