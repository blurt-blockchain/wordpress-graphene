# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.3](https://gitlab.com/blurt-blockchain/wordpress-graphene/compare/v1.0.2...v1.0.3) (2024-03-14)


### Code Refactoring

* **status:** add a status for API & RSS request ([fdebc43](https://gitlab.com/blurt-blockchain/wordpress-graphene/commit/fdebc4308f63e9458881d3ad33a91d4c0bd596c5))

### 1.0.2 (2024-03-13)


### Features

* **show:** show body without " ([62d00ed](https://gitlab.com/blurt-blockchain/wordpress-graphene/commit/62d00ed73dff983f8e2b064d2699feb594878177))
