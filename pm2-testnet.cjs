module.exports = {
  apps: [
    {
      name: 'WPG-TESTNET',
      script: './dist/index.js',
      // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
      args: '-env TESTNET -p 3002',
      instances: 1,
      autorestart: true,
      watch: false,
      max_memory_restart: '5G',
    },
  ],
};

